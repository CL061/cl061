#include<stdio.h>
int input()
{
    int n;
    printf("enter the number of elements in the array\n");
    scanf("%d",&n);
    return n;
}
void input_array(int n)
{
    int a[n];
    for(int i=0;i<n;i++)
    {
        printf("enter the numbers\n");
        scanf("%d",&a[i]);
    }
}
float find_sum(int n, int a[n])
{
    float sum=0;
    for(int i=0;i<n;i++)
    {
        sum+=i;
    }
    return sum;
}
float average(int n, float sum)
{
    float average;
    average=sum/n;
    return average;
}
void display(int n, int a[n], float sum, float average)
{
    printf("the sum is %f\n",sum);
    printf("the average is %f\n",average);
}
int main()
{
    int n;
    float b,c;
    n=input();
    int a[n];
    input_array(n);
    b=find_sum(n,a);
    c=average(n,b);
    display(n,a,b,c);
    return 0;
}
