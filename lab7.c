#include<stdio.h>
int input()
{
    int n; 
    printf("enter the length of the string\n");
    scanf("%d",&n);
    return n;
}
void input_array(int n, char str[n])
{
    printf("enter the members of the string\n");
    for(int i=0; i<=n;i++)
    {
        scanf("%c",&str[i]);
    }
}
void reverse_string(int n, char str[n], char revstr[n])
{
    int j=0;
    for(int i=n;i>=0;i--)
    {
        revstr[j]=str[i];
        j=j+1;
    }
    revstr[j]='\0';
}
int compare(int n, char str[n], char revstr[n])
{
    int flag=0;
    for(int i=0;i<=n;i++)
    {
        if(str[i]==revstr[i])
        {
            flag+=1;
        }
    }
    return flag;
}
void display (int n, char str[n], char revstr[n], int flag)
{
    printf("the entered string is: ");
    for(int i=0; i<=n;i++)
    {
        printf("%c",str[i]);
    }
    printf("\nthe reverse string is: ");
    for(int i=0; i<=n;i++)
    {
        printf("%c",revstr[i]);
    }
    if(flag==0)
    {
        printf("\nthe string is a palindome\n");
    }
    else
    {
        printf("\nthe string is not a palindrome\n");
    }
}
int main()
{
    int n; 
    n=input();
    char str[n], revstr[n];
    input_array(n,str);
    reverse_string(n,str,revstr);
    int f;
    f=compare(n, str, revstr);
    display(n, str, revstr,f);
    return 0;
}
