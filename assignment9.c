#include <stdio.h>
struct student 
{
    char name[50];
    int roll;
    float marks;
} s[10];

int main() 
{
    int i;
    printf("Enter information of students:\n");
    for (i = 0; i < 10; i++) 
    {
        printf("\nEnter the student-%d details,\n",i+1);
        printf("Enter name: ");
        scanf("%s", s[i].name);
        printf("Enter roll number: ");
        scanf("%d", &s[i].roll);
        printf("Enter marks: ");
        scanf("%f", &s[i].marks);
    }
    printf("Displaying Information:\n\n");
    for (i = 0; i < 10; i++)
    {
        printf("\nName:%s",s[i].name);
        printf("\nRoll number:%d",s[i].roll);
        printf("\nMarks: %f", s[i].marks);
    }
    return 0;
}
