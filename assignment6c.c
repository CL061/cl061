#include <stdio.h>
int input()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}

void input_array(int n)
{
    int a[n];
    for(int i=0;i<n;i++)
    {
        printf("Enter the element no %d of the array\n",i);
        scanf("%d",&a[i]);
    }
}
int define()
{
    int x;
    printf("enter the number whose position is to be found\n");
    scanf("%d",&x);
    return x;
}
int find_pos(int n, int a[n], int x)
{
    int pos;
    for(int i=0;i<n;i++)
    {
        if(a[i]=x)
        pos=i;
    }
    return pos;
}
void display(int n, int a[n], int pos)
{
    printf("The position is %d\n",pos);
}
int main()
{
    int n,p,b;
    n=input();
    int a[n];
    input_array(n);
    p=define();
    b=find_pos(n,a,p);
    display(n,a,b);
    return 0;
}

