#include<stdio.h>
#include<string.h>
int
main ()
{
  struct pub_dat
  {
    int day;
    int month;
    int year;
  };
  struct b_det
  {
    char book_name[50];
    char author_name[50];
    struct pub_dat date;
    int tot_pages;
    char pub_by[50];
  };
  struct b_det book1;
  printf ("\n Enter the boook  name : ");
  scanf ("%s", book1.book_name);
  printf ("\n Enter the author_name  name : ");
  scanf ("%s", book1.author_name);
  printf ("\n Enter the published date : ");
  scanf ("%d %d %d", &book1.date.day, &book1.date.month, &book1.date.year);
  printf ("\n Enter total number of pages: ");
  scanf ("%d", &book1.tot_pages);
  printf ("\n Enter the publication  name : ");
  scanf ("%s", book1.pub_by);
  printf ("\n ********BOOK DETAILS *******");
  printf ("\n Book Name    : %s", book1.book_name);
  printf ("\n Written by   : %s", book1.author_name);
  printf ("\n Pulished on  : %d/%d/%d", book1.date.day, book1.date.month,
	  book1.date.year);
  printf ("\n Total pages  : %d", book1.tot_pages);
  printf ("\n Published by : %s", book1.pub_by);
  return 0;
}
