#include <stdio.h>
int main()
{
    char str[100];
    int i=0;
    printf("\n Enter the string :");
    gets(str);
    while(str[i]!='\0')
    {
        if(str[i]>='a' && str[i]<='z')
        {
            str[i] = str[i]-32;
        }
        else
        {
            str[i] = str[i]+32;
        }
        i++;
    }
    str[i] = '\0';
    printf("\n The string converted into upper case is : ");
    puts(str);
    return 0;
}
