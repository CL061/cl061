#include<stdio.h>
int gcd(int,int);
int main()
{
    int n1,n2;
    printf("Enter two numbers:\n");
    scanf("%d %d",&n1,&n2);
    printf("GCD of %d and %d is=%d",n1,n2,gcd(n1,n2));
    return 0;
}
int gcd(int x,int y)
{
    if(x==0)
        return y;
    while(y!=0)
    {
        if(x>y)
            x=x-y;
        else
            y=y-x;
    }
    return x;
}
