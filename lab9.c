#include<stdio.h>
int main()
{
	int a,b,*p,*q,add,sub,multi,rem;
	float div;
	printf("Enter the two integers\n");
	scanf("%d%d",&a,&b);
	p = &a;
	q = &b;
	add = *p+*q;
	sub = *p-*q;
	multi = *p*(*q);
	div = *p/(*q) ;
	rem = *p%(*q) ;
	printf("\nThe sum of the two integers %d and %d are %d:",*p,*q,add);
	printf("\nThe difference of the two integers %d and %d are %d:",*p,*q,sub);
	printf("\nThe product of the two integers %d and %d are %d:",*p,*q,multi);
	printf("\nThe division of the two integers %d and %d are %f:",*p,*q,div);
	printf("\nThe reminder of the two integers %d and %d are %d:",*p,*q,rem);
	return 0;
}
